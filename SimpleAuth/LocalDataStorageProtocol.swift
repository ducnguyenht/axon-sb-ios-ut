//
//  LocalDataStorageProtocol.swift
//  SimpleAuth
//
//  Created by dtnhan on 3/4/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import Foundation

protocol LocalDataStorageProtocol {
    func dictionary(forKey defaultName: String) -> [String : Any]?
    func set(_ value: Any?, forKey defaultName: String)
}

extension UserDefaults: LocalDataStorageProtocol { }
