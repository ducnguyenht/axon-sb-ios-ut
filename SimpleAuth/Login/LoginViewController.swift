//
//  LoginViewController.swift
//  SimpleAuth
//
//  Created by dtnhan on 2/20/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import UIKit
import Combine

class LoginViewController: UIViewController {
    @IBOutlet private weak var loginView: UIView!
    @IBOutlet private weak var usernameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var loginViewBottom: NSLayoutConstraint!
    
    private(set) var viewModel = LoginViewModel(userRepository: UserRepository())
    private var routeBinder: AnyCancellable!
    private var errorBinder: AnyCancellable!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        bindViewModel()
        viewModel.verifyLoggedInStatus()
    }
    
    private func bindViewModel() {
        routeBinder = viewModel.output.$route.sink { [weak self] route in
            self?.handle(route: route)
        }
        errorBinder = viewModel.output.$error.sink { [weak self] error in
            self?.handle(error: error)
        }
    }

    @objc func keyboardWillShow(_ notification: Notification) {
        guard let rect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        loginViewBottom.constant = rect.height
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }

    @objc func keyboardWillHide(_ notification: Notification) {
        loginViewBottom.constant = 0
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func login() {
        viewModel.login(username: usernameTextField.text, password: passwordTextField.text)
    }
    
    @IBAction func register() {
        viewModel.openRegister()
    }
    
    @IBAction func unwindToLogin(_ segue: UIStoryboardSegue) {
        
    }
    
    deinit {
        routeBinder.cancel()
        errorBinder.cancel()
    }
}

extension LoginViewController {
    func handle(route: LoginViewRoute) {
        switch route {
        case .initial: break
        case .openRegister: performSegue(withIdentifier: "Register", sender: nil)
        case .switchToProfile: UIApplication.shared.switchToProfile()
        case .showLoginView: loginView.isHidden = false
        }
    }
    
    func handle(error: Error?) {
        guard let message = error?.localizedDescription else { return }
        let controller = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        controller.addAction(title: "OK", style: .cancel)
        present(controller, animated: true)
    }
}
