//
//  LoginViewModel.swift
//  SimpleAuth
//
//  Created by dtnhan on 2/20/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import Foundation

enum LoginViewRoute {
    case initial
    case showLoginView
    case openRegister
    case switchToProfile
}

class LoginViewModel {
    private(set) var output = Output()
    let userRepository: UserRepositoryProtocol
    
    init(userRepository: UserRepositoryProtocol) {
        self.userRepository = userRepository
    }
}

extension LoginViewModel {
    func verifyLoggedInStatus() {
        guard let token = userRepository.currentUser?.accessToken else {
            output.route = .showLoginView
            return
        }
        userRepository.verify(accessToken: token) { [weak self] result in
            switch result {
            case .success(let user):
                self?.userRepository.setCurrentUser(user)
                self?.output.route = .switchToProfile
            case .failure(let error):
                if let error = error as? RemoteError, error.status == 404 {
                    self?.userRepository.setCurrentUser(nil)
                    self?.output.route = .showLoginView
                }
                self?.output.error = error
            }
        }
    }
    
    func login(username: String?, password: String?) {
        guard let username = username, !username.isEmpty else {
            output.error = AppError.inputError(reason: .invalidUsername)
            return
        }
        guard let password = password, !password.isEmpty else {
            output.error = AppError.inputError(reason: .invalidPassword)
            return
        }
        userRepository.login(username: username, password: password) { [weak self] result in
            switch result {
            case .success(let user):
                self?.userRepository.setCurrentUser(user)
                self?.output.route = .switchToProfile
            case .failure(let error):
                self?.output.error = error
            }
        }
    }
    
    func openRegister() {
        output.route = .openRegister
    }
}

extension LoginViewModel {
    class Output {
        @Published var route: LoginViewRoute = .initial
        @Published var error: Error?
    }
}
