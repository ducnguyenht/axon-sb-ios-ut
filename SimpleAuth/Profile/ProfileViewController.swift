//
//  ProfileViewController.swift
//  SimpleAuth
//
//  Created by dtnhan on 2/20/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import UIKit
import Combine

class ProfileViewController: UIViewController {
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var usernameLabel: UILabel!
    
    private(set) var viewModel = ProfileViewModel(userRepository: UserRepository())
    private var routeBinder: AnyCancellable!
    private var usernameBinder: AnyCancellable!
    private var avatarUrlBinder: AnyCancellable!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }
    
    private func bindViewModel() {
        routeBinder = viewModel.output.$route.sink { [weak self] route in
            self?.handle(route: route)
        }
        usernameBinder = viewModel.output.$username.sink { [weak self] username in
            self?.usernameLabel.text = username
        }
        avatarUrlBinder = viewModel.output.$avatarUrl.sink { [weak self] avatarUrl in
            self?.avatarImageView.setImageURL(avatarUrl)
        }
    }
    
    @IBAction func logout() {
        viewModel.logout()
    }
    
    deinit {
        routeBinder.cancel()
        usernameBinder.cancel()
        avatarUrlBinder.cancel()
    }
}

extension ProfileViewController {
    func handle(route: ProfileViewRoute) {
        switch route {
        case .initial: break
        case .switchToLogin: UIApplication.shared.switchToLogin()
        }
    }
}
