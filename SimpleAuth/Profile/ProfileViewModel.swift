//
//  ProfileViewModel.swift
//  SimpleAuth
//
//  Created by dtnhan on 2/20/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import Foundation

enum ProfileViewRoute {
    case initial
    case switchToLogin
}

class ProfileViewModel {
    private(set) var output = Output()
    var userRepository: UserRepositoryProtocol
    
    init(userRepository: UserRepositoryProtocol) {
        self.userRepository = userRepository
        if let user = userRepository.currentUser {
            updateProfile(user)
        }
    }
    
    private func updateProfile(_ user: UserProfile) {
        self.output.username = user.username
        self.output.avatarUrl = URL(string: user.avatar)
    }
}

extension ProfileViewModel {
    func logout() {
        userRepository.logout { [weak self] result in
            if result.isSuccess {
                self?.userRepository.setCurrentUser(nil)
                self?.output.route = .switchToLogin
            }
       }
    }
}

extension ProfileViewModel {
    class Output {
        @Published var route: ProfileViewRoute = .initial
        @Published var username: String?
        @Published var avatarUrl: URL?
    }
}
