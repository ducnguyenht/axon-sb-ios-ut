//
//  RegisterViewController.swift
//  SimpleAuth
//
//  Created by dtnhan on 2/21/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import UIKit
import Combine

class RegisterViewController: UIViewController {
    @IBOutlet private weak var usernameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var registerViewBottom: NSLayoutConstraint!
    
    private(set) var viewModel = RegisterViewModel(userRepository: UserRepository())
    private var routeBinder: AnyCancellable!
    private var messageBinder: AnyCancellable!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        bindViewModel()
    }
    
    private func bindViewModel() {
        routeBinder = viewModel.output.$route.sink { [weak self] route in
            self?.handle(route: route)
        }
        messageBinder = viewModel.output.$message.sink { [weak self] message in
            self?.handle(message: message)
        }
    }

    @objc func keyboardWillShow(_ notification: Notification) {
        guard let rect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        registerViewBottom.constant = rect.height
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }

    @objc func keyboardWillHide(_ notification: Notification) {
        registerViewBottom.constant = 0
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func register() {
        viewModel.register(username: usernameTextField.text, password: passwordTextField.text)
    }
    
    @IBAction func back() {
        viewModel.openLogin()
    }
    
    deinit {
        routeBinder.cancel()
        messageBinder.cancel()
    }
}

extension RegisterViewController {
    func handle(route: RegisterViewRoute) {
        switch route {
        case .initial: break
        case .openLogin: performSegue(withIdentifier: "Login", sender: nil)
        }
    }
    
    func handle(message: String?) {
        guard let message = message else { return }
        let controller = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        controller.addAction(title: "OK", style: .cancel)
        present(controller, animated: true)
    }
}
