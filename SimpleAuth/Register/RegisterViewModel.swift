//
//  RegisterViewModel.swift
//  SimpleAuth
//
//  Created by dtnhan on 2/21/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import Foundation

enum RegisterViewRoute {
    case initial
    case openLogin
}

class RegisterViewModel {
    private(set) var output = Output()
    let userRepository: UserRepositoryProtocol
    
    init(userRepository: UserRepositoryProtocol) {
        self.userRepository = userRepository
    }
}

extension RegisterViewModel {
    func register(username: String?, password: String?) {
        guard let username = username, username.isValidUsername else {
            output.message = AppError.inputError(reason: .invalidUsername).localizedDescription
            return
        }
        guard let password = password, password.isValidPassword else {
            output.message = AppError.inputError(reason: .invalidPassword).localizedDescription
            return
        }
        userRepository.createAccount(username: username, password: password) { [weak self] result in
            self?.output.message = result.error?.localizedDescription ?? "Register successfully"
        }
    }
    
    func openLogin() {
        output.route = .openLogin
    }
}

extension RegisterViewModel {
    class Output {
        @Published var route: RegisterViewRoute = .initial
        @Published var message: String?
    }
}
