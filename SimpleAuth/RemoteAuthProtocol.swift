//
//  RemoteAuthProtocol.swift
//  SimpleAuth
//
//  Created by dtnhan on 3/4/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import SimpleAuthSDK

protocol RemoteAuthProtocol {
    func register(username: String, password: String, completion: @escaping (Data) -> Void)
    func login(username: String, password: String, completion: @escaping (Data) -> Void)
    func verify(accessToken: String, completion: @escaping (Data) -> Void)
}

extension Auth: RemoteAuthProtocol { }
