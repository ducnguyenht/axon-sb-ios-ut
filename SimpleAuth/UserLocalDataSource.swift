//
//  UserLocalDataSource.swift
//  SimpleAuth
//
//  Created by dtnhan on 2/20/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import Foundation

class UserLocalDataSource {
    let storage: LocalDataStorageProtocol
    
    init(storage: LocalDataStorageProtocol = UserDefaults.standard) {
        self.storage = storage
        user = UserProfile.user(from: storage.dictionary(forKey: "UserLocalDataSource_user"))
    }
    
    private(set) var user: UserProfile? {
        willSet {
            storage.set(newValue?.dict, forKey: "UserLocalDataSource_user")
        }
    }
    
    func save(user: UserProfile?) {
        self.user = user
    }
}
