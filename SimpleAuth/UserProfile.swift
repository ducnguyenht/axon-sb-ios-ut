//
//  UserProfile.swift
//  SimpleAuth
//
//  Created by dtnhan on 3/3/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import Foundation

struct UserProfile: Codable {
    let username: String
    let avatar: String
    let accessToken: String
    
    var dict: [String: Any] {
        return ["username": username, "avatar": avatar, "accessToken": accessToken]
    }
    
    enum CodingKeys: String, CodingKey {
        case username
        case avatar
        case accessToken = "access_token"
    }
    
    static func user(from dict: [String: Any]?) -> UserProfile? {
        guard let dict = dict,
            let username = dict["username"] as? String,
            let avatar = dict["avatar"] as? String,
            let accessToken = dict["accessToken"] as? String else { return nil }
        return UserProfile(username: username, avatar: avatar, accessToken: accessToken)
    }
}
