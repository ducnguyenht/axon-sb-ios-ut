//
//  UserRemoteDataSource.swift
//  SimpleAuth
//
//  Created by dtnhan on 2/20/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import Foundation
import SimpleAuthSDK

class UserRemoteDataSource {
    let auth: RemoteAuthProtocol
    
    init(auth: RemoteAuthProtocol = Auth.auth) {
        self.auth = auth
    }
    
    func createAccount(username: String, password: String, completion: @escaping ResultWithoutDataHandler) {
        let decoder: (Data) throws -> Void = { data in
            let _ = try JSONDecoder().decode(UserProfile.self, from: data)
        }
        auth.register(username: username, password: password) {
            completion(UserRemoteDataSource.parse(decoder: decoder, data: $0))
        }
    }
    
    func verify(accessToken: String, completion: @escaping ResultHandler<UserProfile>) {
        let decoder: (Data) throws -> UserProfile = { data in
            let response = try JSONDecoder().decode(UserProfile.self, from: data)
            return UserProfile(username: response.username, avatar: response.avatar, accessToken: response.accessToken)
        }
        auth.verify(accessToken: accessToken) {
            completion(UserRemoteDataSource.parse(decoder: decoder, data: $0))
        }
    }
    
    func login(username: String, password: String, completion: @escaping ResultHandler<UserProfile>) {
        let decoder: (Data) throws -> UserProfile = { data in
            let response = try JSONDecoder().decode(UserProfile.self, from: data)
            return UserProfile(username: response.username, avatar: response.avatar, accessToken: response.accessToken)
        }
        auth.login(username: username, password: password) {
            completion(UserRemoteDataSource.parse(decoder: decoder, data: $0))
        }
    }
    
    func logout(completion: ResultWithoutDataHandler) {
        completion(.successWithoutData)
    }
}

extension UserRemoteDataSource {
    static func parse<T>(decoder: (Data) throws -> T, data: Data) -> Result<T> {
        let jsonDecoder = JSONDecoder()
        do {
            return .success(try decoder(data))
        } catch {
            return .failure(try! jsonDecoder.decode(RemoteError.self, from: data))
        }
    }
}

