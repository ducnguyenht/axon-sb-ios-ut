//
//  UserRepository.swift
//  SimpleAuth
//
//  Created by dtnhan on 2/20/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import Foundation

protocol UserRepositoryProtocol {
    var currentUser: UserProfile? { get }
    func setCurrentUser(_ user: UserProfile?)
    func createAccount(username: String, password: String, completion: @escaping ResultWithoutDataHandler)
    func verify(accessToken: String, completion: @escaping ResultHandler<UserProfile>)
    func login(username: String, password: String, completion: @escaping ResultHandler<UserProfile>)
    func logout(completion: @escaping ResultWithoutDataHandler)
}

class UserRepository: UserRepositoryProtocol {
    let localDataSource: UserLocalDataSource
    let remoteDataSource: UserRemoteDataSource
    
    init(localDataSource: UserLocalDataSource = UserLocalDataSource(), remoteDataSource: UserRemoteDataSource = UserRemoteDataSource()) {
        self.localDataSource = localDataSource
        self.remoteDataSource = remoteDataSource
    }
    
    var currentUser: UserProfile? {
        return localDataSource.user
    }
    
    func setCurrentUser(_ user: UserProfile?) {
        localDataSource.save(user: user)
    }
    
    func createAccount(username: String, password: String, completion: @escaping ResultWithoutDataHandler) {
        remoteDataSource.createAccount(username: username, password: password) { result in
            completion(result.withoutData)
        }
    }
    
    func verify(accessToken: String, completion: @escaping ResultHandler<UserProfile>) {
        remoteDataSource.verify(accessToken: accessToken, completion: completion)
    }
    
    func login(username: String, password: String, completion: @escaping ResultHandler<UserProfile>) {
        remoteDataSource.login(username: username, password: password, completion: completion)
    }
    
    func logout(completion: @escaping ResultWithoutDataHandler) {
        remoteDataSource.logout(completion: completion)
    }
}
