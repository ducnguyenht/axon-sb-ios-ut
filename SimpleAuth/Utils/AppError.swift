//
//  AppError.swift
//  SimpleAuth
//
//  Created by dtnhan on 3/3/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import Foundation

protocol AppErrorReason: CustomDebugStringConvertible { }

enum AppError {
    case generalError(reason: GeneralReason)
    case inputError(reason: InputReason)
    
    var reason: AppErrorReason {
        switch self {
        case .generalError(let reason):
            return reason
        case .inputError(let reason):
            return reason
        }
    }
}

extension AppError: LocalizedError {
    var errorDescription: String? {
        return reason.debugDescription
    }
}

extension AppError {
    enum GeneralReason {
        case screenNotFound(name: String)
        case unknown
    }
}

extension AppError.GeneralReason: AppErrorReason {
    var debugDescription: String {
        switch self {
        case .screenNotFound(let name):
            return "\(name) not found!"
        case .unknown:
            return "Unknown error"
        }
    }
}

extension AppError {
    enum InputReason {
        case invalidUsername
        case invalidPassword
    }
}

extension AppError.InputReason: AppErrorReason {
    var debugDescription: String {
        switch self {
        case .invalidUsername:
            return "Invalid username"
        case .invalidPassword:
            return "Invalid password"
        }
    }
}
