//
//  UIAlertController.swift
//  SimpleAuth
//
//  Created by Kent DANG on 2/23/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import UIKit

extension UIAlertController {
    func addAction(title: String, style: UIAlertAction.Style, handler: ((UIAlertAction) -> Void)? = nil) {
        addAction(UIAlertAction(title: title, style: style, handler: handler))
    }
}
