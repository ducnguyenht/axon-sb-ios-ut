//
//  UIApplication.swift
//  SimpleAuth
//
//  Created by dtnhan on 3/2/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import UIKit

extension UIApplication {
    func switchToProfile() {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ProfileViewController")
        guard let window = delegate!.window!, !(window.rootViewController is ProfileViewController) else { return }
        window.rootViewController = controller
    }
    
    func switchToLogin() {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "LoginViewController")
        guard let window = delegate!.window!, !(window.rootViewController is LoginViewController) else { return }
        window.rootViewController = controller
    }
}
