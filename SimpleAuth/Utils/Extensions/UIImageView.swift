//
//  UIImageView.swift
//  SimpleAuth
//
//  Created by dtnhan on 3/3/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import UIKit

extension UIImageView {
    func setImageURL(_ url: URL?) {
        DispatchQueue.global().async { [weak self] in
            guard let url = url, let data = try? Data(contentsOf: url) else {
                self?.setImageData(nil)
                return
            }
            self?.setImageData(data)
        }
    }
    
    private func setImageData(_ data: Data?) {
        guard Thread.isMainThread else {
            DispatchQueue.main.async {
                self.setImageData(data)
            }
            return
        }
        if let data = data, let image = UIImage(data: data) {
            self.image = image
        } else {
            self.image = nil
        }
    }
}
