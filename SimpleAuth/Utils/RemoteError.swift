//
//  RemoteError.swift
//  SimpleAuth
//
//  Created by dtnhan on 3/3/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import Foundation

struct RemoteError: Codable {
    let status: Int
    let message: String
    
    static let noData = RemoteError(status: 999, message: "No data")
}

extension RemoteError: LocalizedError {
    var errorDescription: String? {
        return message
    }
}
