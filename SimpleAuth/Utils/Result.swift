//
//  Result.swift
//  SimpleAuth
//
//  Created by dtnhan on 3/3/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
    
    var error: Error? {
        guard case let .failure(error) = self else { return nil }
        return error
    }
    
    var isSuccess: Bool {
        return error == nil
    }
    
    var withoutData: ResultWithoutData {
        switch self {
        case .success:
            return .successWithoutData
        case .failure(let error):
            return .failure(error)
        }
    }
    
    static var successWithoutData: ResultWithoutData {
        return .success(())
    }
}

typealias ResultHandler<T> = (Result<T>) -> Void
typealias ResultWithoutData = Result<Void>
typealias ResultWithoutDataHandler = (ResultWithoutData) -> Void
