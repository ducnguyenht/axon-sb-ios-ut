//
//  AppError.swift
//  SimpleAuthTests
//
//  Created by dtnhan on 3/3/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import XCTest
@testable import SimpleAuth

class AppErrorTests: XCTestCase {
    // Verify that screenNotFound error has correct localizedDescription
    func test_GeneralError_screenNotFound() {
    
    }
    
    // Verify that unknown error has correct localizedDescription
    func test_GeneralError_unknown() {
        
    }
    
    // Verify that invalidUsername error has correct localizedDescription
    func test_InputError_invalidUsername() {
        
    }
    
    // Verify that invalidPassword error has correct localizedDescription
    func test_InputError_invalidPassword() {
        
    }
}
