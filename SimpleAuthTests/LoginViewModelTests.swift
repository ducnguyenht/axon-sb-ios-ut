//
//  LoginViewModelTests.swift
//  SimpleAuthTests
//
//  Created by dtnhan on 3/3/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import XCTest
@testable import SimpleAuth

// Username must be email only
// Password must be a string which contains 8 or more ASCII-standard characters
class LoginViewModelTests: XCTestCase {
    var viewModel: LoginViewModel!
    
    override func setUp() {
        super.setUp()
    }
    
    // Verify that viewModel will route to login view if no access token
    func test_Verify_no_access_token() {
        
    }
    
    // Verify that viewModel will clear current user and route to login view when verify access token but get invalid token error
    func test_Verify_get_invalid_token_error() {
        
    }
    
    // Verify that viewModel will not clear current user, stay at current screen and request to show an error when verify access token but get an error which is not invalid token error
    func test_Verify_get_error() {
        
    }
    
    // Verify that viewModel will save latest user profile and route to profile screen when verify access token success
    func test_Verify_success() {
        
    }
    
    // Verify that viewModel will request to show an error with reason invalid username if login with nil username
    func test_Login_with_nil_username() {
        
    }
    
    // Verify that viewModel will request to show an error with reason invalid username if login with empty username
    func test_Login_with_empty_username() {
        
    }
    
    // Verify that viewModel will request to show an error with reason invalid password if login with nil password
    func test_Login_with_nil_password() {
        
    }
    
    // Verify that viewModel will request to show an error with reason invalid username if login with empty password
    func test_Login_with_empty_password() {
        
    }
    
    // Verify that viewModel will stay at last screen and request to show an error if login with valid format input but the remote returns an error
    func test_Login_get_error() {
        
    }
    
    // Verify that viewModel will save current user correctly and route to profile screen when login success
    func test_Login_success() {
        
    }
    
    // Verify that viewModel will route to register screen if request it open login
    func test_OpenRegister() {
        
    }
    
    override func tearDown() {
        super.tearDown()
    }
}
