//
//  RemoteErrorTests.swift
//  SimpleAuthTests
//
//  Created by dtnhan on 3/3/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import XCTest
@testable import SimpleAuth

class RemoteErrorTests: XCTestCase {
    // Verify that RemoteError has correct status and localizedDescription
    func test_Initialization() {
        
    }
    
    // Verify that noData error has correct localizedDescription
    func test_NoData() {
        
    }
}
