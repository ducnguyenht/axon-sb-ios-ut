//
//  ProfileViewModelTests.swift
//  SimpleAuthTests
//
//  Created by dtnhan on 3/3/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import XCTest
@testable import SimpleAuth

class ProfileViewModelTests: XCTestCase {
    var viewModel: ProfileViewModel!
    
    override func setUp() {
        super.setUp()
    }
    
    // Verify that viewModel will clear current user and route to login screen when logout success
    func test_Logout_success() {
        
    }
    
    override func tearDown() {
        super.tearDown()
    }
}
