//
//  RegisterViewModelTests.swift
//  SimpleAuthTests
//
//  Created by dtnhan on 3/3/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import XCTest
@testable import SimpleAuth

// Username must be email only
// Password must be a string which contains 8 or more ASCII-standard characters
class RegisterViewModelTests: XCTestCase {
    var viewModel: RegisterViewModel!
    
    override func setUp() {
        super.setUp()
    }
    
    // Verify that viewModel will show message `Invalid username` if register with nil username
    func test_Register_with_nil_username() {
        
    }
    
    // Verify that viewModel will show message `Invalid username` if register with empty username
    func test_Register_with_empty_username() {
        
    }
    
    // Verify that viewModel will show message `Invalid username` if register with invalid format username
    func test_Register_with_invalid_username() {
        
    }
    
    // Verify that viewModel will show message `Invalid password` if register with nil password
    func test_Register_with_nil_password() {
        
    }
    
    // Verify that viewModel will show message `Invalid password` if register with empty password
    func test_Register_with_empty_password() {
        
    }
    
    // Verify that viewModel will show message `Invalid password` if register with a password has 7 ASCII-standard characters
    func test_Register_with_not_enough_length_password() {
        
    }
    
    // Verify that viewModel will show message `Invalid password` if register with a password has non-ASCII-standard characters
    func test_Register_with_invalid_password() {
        
    }
    
    // Verify that viewModel will message a error description if register with valid format input but the remote returns an error
    func test_Register_with_valid_input_get_error() {
        
    }
    
    // Verify that viewModel will show message `Register successfully` when register success
    func test_Register_success() {
        
    }
    
    // Verify that viewModel will route to login screen if request it open login
    func test_OpenLogin() {
        
    }
    
    override func tearDown() {
        super.tearDown()
    }
}
