//
//  ResultTests.swift
//  SimpleAuthTests
//
//  Created by dtnhan on 3/4/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import XCTest
@testable import SimpleAuth

class ResultTests: XCTestCase {
    // Verify that failure result works properly
    func test_Result_fail() {
        
    }
    
    // Verify that success result works properly
    func test_Result_success() {
        
    }
    
    // Verify that failure result without data works properly
    func test_ResultWithoutData_fail() {
        
    }
    
    // Verify that success result without data works properly
    func test_ResultWithoutData_success() {
        
    }
    
    // Verify that result without data returned from other result works properly
    func test_Transform_Result_to_ResultWithoutData() {
        
    }
}
