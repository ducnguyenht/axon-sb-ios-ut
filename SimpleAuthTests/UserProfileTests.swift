//
//  UserProfileTests.swift
//  SimpleAuthTests
//
//  Created by dtnhan on 3/3/20.
//  Copyright © 2020 Axon Active VN. All rights reserved.
//

import XCTest
@testable import SimpleAuth

class UserProfileTests: XCTestCase {
    // Verify that cannot create user profile instance with nil dictionary
    func test_Init_from_nil_dictionary() {
        
    }
    
    // Verify that cannot create user profile instance with empty dictionary
    func test_Init_from_empty_dictionary() {
        
    }
    
    // Verify that cannot create user profile instance with invalid dictionary
    func test_Init_from_invalid_dictionary() {
        
    }
    
    // Verify that can create user profile instance with valid dictionary
    func test_Init_from_valid_dictionary() {
        
    }
}
